package io.gitlab.arturbosch.kanbin.tinbo;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * @author Artur Bosch
 */
class UtilTest {

	@Test
	void splitOrderIsRight() {
		Set<String> expected = new LinkedHashSet<>(Arrays.asList("Backlog", "Doing", "Done"));
		Set<String> actual = Util.split("Backlog,Doing,Done", ",");
		Assertions.assertEquals(actual, expected);
	}
}
