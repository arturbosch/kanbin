package io.gitlab.arturbosch.kanbin.tinbo;

import com.google.common.collect.Sets;
import io.gitlab.arturbosch.kanbin.models.Card;
import io.gitlab.arturbosch.kanbin.models.Lane;
import io.gitlab.arturbosch.kanbin.models.LaneSettings;
import io.gitlab.arturbosch.kanbin.models.Project;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * @author Artur Bosch
 */
class PrintBoardTableTest {

	/**
	 * Backlog|Progress|Finished
	 * -------------------------
	 */
	@Test
	void justHeadersNoCards() {
		Project project = new Project("Test", Sets.newHashSet(
				new Lane("First", new LaneSettings(0)),
				new Lane("Second", new LaneSettings(1)),
				new Lane("Third", new LaneSettings(2))));

		PrintBoardTable table = new PrintBoardTable(project);
		String result = table.toString();
		System.out.println(result);

		Assertions.assertTrue(result.contains("First"));
		Assertions.assertTrue(result.contains("Second"));
		Assertions.assertTrue(result.contains("Third"));
	}

	/**
	 * Backlog|Progress|Finished
	 * -------------------------
	 * one    |        | omg
	 * _	  |        | bla
	 * _	  |        | two
	 * _	  |        | three
	 */
	@Test
	void threeLanesOneCardZeroCardTwoCards() {
		Lane backlog = new Lane("Backlog", new LaneSettings(0));
		Lane finished = new Lane("Finished", new LaneSettings(2));
		Project project = new Project("Test", Sets.newHashSet(
				backlog,
				new Lane("Progress", new LaneSettings(1)),
				finished));

		backlog.addCard(new Card("one", "", "Backlog"));
		finished.addCard(new Card("omg", "", "Finished"));
		finished.addCard(new Card("bla", "", "Finished"));
		finished.addCard(new Card("two", "", "Finished"));
		finished.addCard(new Card("three", "", "Finished"));

		PrintBoardTable table = new PrintBoardTable(project);
		String result = table.toString();
		System.out.println(result);

		Assertions.assertTrue(result.split("\n").length == 6);
	}

	/**
	 * Backlog|Progress|Finished
	 * -------------------------
	 * _      | One    |
	 */
	@Test
	void onlyMiddleLaneHasItems() {
		Lane progress = new Lane("Progress", new LaneSettings(1));
		Project project = new Project("Test", Sets.newHashSet(
				new Lane("Backlog", new LaneSettings(0)),
				progress,
				new Lane("Finished", new LaneSettings(2))));

		progress.addCard(new Card("one", "", "Progress"));

		PrintBoardTable table = new PrintBoardTable(project);
		String result = table.toString();
		System.out.println(result);

		Assertions.assertTrue(result.split("\n").length == 3);
	}
}
