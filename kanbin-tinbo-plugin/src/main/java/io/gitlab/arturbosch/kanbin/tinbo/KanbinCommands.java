package io.gitlab.arturbosch.kanbin.tinbo;

import io.gitlab.arturbosch.kanbin.models.Card;
import io.gitlab.arturbosch.kanbin.utils.Pre;
import io.gitlab.arturbosch.kanbin.utils.Try;
import io.gitlab.arturbosch.tinbo.api.TinboTerminal;
import io.gitlab.arturbosch.tinbo.api.config.ModeManager;
import io.gitlab.arturbosch.tinbo.api.marker.Command;
import org.jetbrains.annotations.NotNull;
import org.springframework.shell.core.annotation.CliAvailabilityIndicator;
import org.springframework.shell.core.annotation.CliCommand;
import org.springframework.shell.core.annotation.CliOption;
import org.springframework.stereotype.Component;

import java.util.Set;

/**
 * @author Artur Bosch
 */
@Component
public class KanbinCommands implements Command {

	private final KanbinService service;
	private final TinboTerminal console;
	private final CommonCommandsProvider commons;

	public KanbinCommands(KanbinService service, TinboTerminal console, CommonCommandsProvider commons) {
		this.service = service;
		this.console = console;
		this.commons = commons;
	}

	@NotNull
	@Override
	public String getId() {
		return Constants.KANBIN;
	}

	@CliAvailabilityIndicator({"current", "boards", "createBoard", "loadBoard", "renameBoard",
			"newLane", "renameLane", "removeLane", "newCard", "updateCard", "removeCard", "done"})
	public boolean available() {
		return ModeManager.INSTANCE.isCurrentMode(KanbinMode.getInstance());
	}

	@CliCommand(value = "current", help = "shows current board")
	public String board() {
		return service.showCurrentProject();
	}

	@CliCommand(value = "boards", help = "Shows all available boards.")
	public String boards() {
		return service.showAvailableProjects();
	}

	@CliCommand(value = {"createBoard", "newBoard"}, help = "Creates a new board.")
	public void create(
			@CliOption(key = {"", "name"}, mandatory = true) String projectName,
			@CliOption(key = "lanes", mandatory = true) String lanes) {
		Pre.notEmpty(projectName);
		Pre.notEmpty(lanes);
		Set<String> laneNames = Util.split(lanes, "[,;\\s]");
		Pre.condition(laneNames.size() > 1, () -> "Minimum two lanes expected.");
		service.createProject(projectName, laneNames);
	}

	@CliCommand(value = "loadBoard", help = "Loads a present board.")
	public void load(@CliOption(key = {"", "name"}, mandatory = true) String projectName) {
		Pre.notEmpty(projectName);
		service.loadProject(projectName);
	}

	@CliCommand(value = "renameBoard", help = "Renames the current loaded board.")
	public void move(@CliOption(key = {"", "name"}, mandatory = true) String newName) {
		Pre.notEmpty(newName);
		service.moveProject(newName);
	}

	@CliCommand(value = "newLane", help = "Creates a new Lane.")
	public void addLane(
			@CliOption(key = {"", "name"}, mandatory = true) String name,
			@CliOption(key = "index", unspecifiedDefaultValue = "-1", specifiedDefaultValue = "-1") int index) {
		Pre.notEmpty(name);
		service.withProject(project -> {
			if (index == -1) {
				project.generateLane(name);
			} else {
				project.generateLane(name, index);
			}
		}).thenRun(this::reloadBoard);
	}

	@CliCommand(value = "renameLane", help = "Renames a lane.")
	public void moveLane(
			@CliOption(key = "old", mandatory = true) String oldName,
			@CliOption(key = "new", mandatory = true) String newName) {
		Pre.notEmpty(oldName);
		Pre.notEmpty(newName);
		service.withProject(project -> project.updateLaneName(newName, oldName))
				.thenRun(this::reloadBoard);
	}

	@CliCommand(value = "removeLane", help = "Removes a lane.")
	public void removeLane(
			@CliOption(key = {"", "name"}, mandatory = true) String laneName,
			@CliOption(key = "moveCardsToLaneWithName", mandatory = true) String moveTo) {
		Pre.notEmpty(laneName);
		Pre.notEmpty(moveTo);
		service.withProject(project -> project.removeLane(laneName, moveTo))
				.thenRun(this::reloadBoard);
	}

	@CliCommand(value = "newCard", help = "Creates a new card for given lane.")
	public void addCard(
			@CliOption(key = {"", "title"}, mandatory = true) String title,
			@CliOption(key = "text", unspecifiedDefaultValue = "", specifiedDefaultValue = "") String text,
			@CliOption(key = "lane", unspecifiedDefaultValue = "", specifiedDefaultValue = "") String lane) {
		Pre.notEmpty(title);
		String givenOrFirst = service.existingLaneOrDefault(lane);
		service.withProject(project -> project.generateCard(givenOrFirst, new Card(title, text, givenOrFirst)))
				.thenRun(this::reloadBoard);
	}

	@CliCommand(value = "updateCard", help = "Updates a card which starts with given title.")
	public void updateCardStartsWithTitle(@CliOption(key = {"", "title"}) String title) {
		Pre.notEmpty(title);
		String newTitle = Try.just(() -> console.readLine("Enter new title or empty if same title: "));
		String newText = Try.just(() -> console.readLine("Enter new description or empty if same description: "));
		String newLane = Try.just(() -> console.readLine("Enter new lane to move card to or empty if same lane: "));
		Card newCard = new Card(newTitle, newText, newLane);
		service.findAndUpdateCard(title, newCard)
				.thenRun(this::reloadBoard);
	}

	@CliCommand(value = "removeCard", help = "Deletes a card from a lane")
	public void removeCard(
			@CliOption(key = {"", "title"}, mandatory = true) String title) {
		Pre.notEmpty(title);
		service.findAndRemoveCard(title)
				.thenRun(this::reloadBoard);
	}

	@CliCommand(value = "done", help = "Moves a card matching given title to the last possible lane.")
	public void finishCard(
			@CliOption(key = {"", "title"}, mandatory = true) String title) {
		Pre.notEmpty(title);
		service.findAndFinishCard(title)
				.thenRun(this::reloadBoard);
	}

	@CliCommand(value = "move", help = "Moves a card matching given title to given lane.")
	public void finishCard(
			@CliOption(key = {"", "title"}, mandatory = true) String title,
			@CliOption(key = "lane", mandatory = true) String lane) {
		Pre.notEmpty(title, lane);
		service.findAndUpdateCard(title, new Card("", "", lane))
				.thenRun(this::reloadBoard);
	}

	@CliCommand(value = "archive", help = "Archives all cards in last lane.")
	public void archiveLastLane(
			@CliOption(key = "lane") String lane) {
		if (Pre.isEmpty(lane)) {
			service.archiveLastLane();
		} else {
			service.archiveLane(lane);
		}
		reloadBoard();
	}

	private void reloadBoard() {
		commons.clear();
		console.write(commons.list());
	}
}
