package io.gitlab.arturbosch.kanbin.tinbo;

import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Artur Bosch
 */
public final class Util {

	private Util() {
	}

	@NotNull
	public static Set<String> split(@NotNull String content, @NotNull String delimiter) {
		return Arrays.stream(content.split(delimiter))
				.map(String::trim)
				.filter(s -> !s.isEmpty())
				.collect(Collectors.toCollection(LinkedHashSet::new));
	}

	@NotNull
	public static <T> T lastElement(@NotNull List<T> collection) {
		if (collection.isEmpty()) {
			throw new NoSuchElementException("Unexpected empty collection.");
		}
		return collection.get(collection.size() - 1);
	}
}
