package io.gitlab.arturbosch.kanbin.tinbo;

import com.google.auto.service.AutoService;
import com.google.common.eventbus.Subscribe;
import io.gitlab.arturbosch.kanbin.formats.KanbinFormat;
import io.gitlab.arturbosch.tinbo.api.TinboBus;
import io.gitlab.arturbosch.tinbo.api.TinboTerminal;
import io.gitlab.arturbosch.tinbo.api.config.HomeFolder;
import io.gitlab.arturbosch.tinbo.api.config.TinboConfig;
import io.gitlab.arturbosch.tinbo.api.config.TinboMode;
import io.gitlab.arturbosch.tinbo.api.marker.Command;
import io.gitlab.arturbosch.tinbo.api.plugins.TinboContext;
import io.gitlab.arturbosch.tinbo.api.plugins.TinboPlugin;
import io.gitlab.arturbosch.tinbo.api.utils.PrintersKt;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.shell.standard.commands.Clear;
import org.springframework.stereotype.Component;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Artur Bosch
 */
@AutoService(TinboPlugin.class)
@Component
public class KanbinPlugin extends TinboPlugin {

	private static final String LAST_USED_PROJECT = "last-used";
	private static final String RELATIVE_PATH = "relative-root-path-inside-tinbo-folder";
	private static final String ABSOLUTE_PATH = "absolute-root-path";

	@Override
	public String version() {
		return "1.0.0";
	}

	@Override
	public @Nullable TinboMode providesMode() {
		return KanbinMode.getInstance();
	}

	@NotNull
	@Override
	public List<Command> registerCommands(@NotNull TinboContext tinbo) {
		KanbinService service = configureService(tinbo.getConfig());
		TinboTerminal console = tinbo.beanOf(TinboTerminal.class);
		Clear clear = tinbo.beanOf(Clear.class);
		CommonCommandsProvider adaptedCommands = new CommonCommandsProvider(service, console, clear);
		KanbinCommands kanbinCommands = new KanbinCommands(service, console, adaptedCommands);
		StartKanbinCommand startKanbinCommand = new StartKanbinCommand();

		List<Command> commands = Arrays.asList(kanbinCommands, adaptedCommands, startKanbinCommand);
		tinbo.registerSingletons(commands);
		return commands;
	}

	private KanbinService configureService(TinboConfig config) {
		Map<String, String> properties = loadOrCreateProperties(config);
		Path basePath = determineKanbinRootPath(properties);
		KanbinService service = new KanbinService(basePath, KanbinFormat.INSTANCE);
		Object subscriber = new Object() {
			@Subscribe
			public void writeLastUsed(LastUsedEvent event) {
				config.writeLastUsed(Constants.KANBIN, event.boardName());
			}
		};
		TinboBus.INSTANCE.register(subscriber);
		initService(properties, service);
		return service;
	}

	private Map<String, String> loadOrCreateProperties(TinboConfig config) {
		Map<String, String> properties = config.getKey(Constants.KANBIN);
		if (properties.isEmpty()) {
			Map<String, String> map = new LinkedHashMap<>();
			map.put(LAST_USED_PROJECT, "");
			map.put(ABSOLUTE_PATH, "");
			map.put(RELATIVE_PATH, Constants.KANBIN);
			config.put(Constants.KANBIN, map);
			config.writeToFile();
			properties = config.getKey(Constants.KANBIN);
		}
		return properties;
	}

	private void initService(Map<String, String> properties, KanbinService service) {
		String lastProject = properties.getOrDefault(LAST_USED_PROJECT, "");
		if (!lastProject.isEmpty()) {
			service.init(lastProject);
		}
	}

	private Path determineKanbinRootPath(Map<String, String> properties) {
		String absolute = properties.getOrDefault(ABSOLUTE_PATH, "");
		String relative = properties.getOrDefault(RELATIVE_PATH, "");
		Path tinboHome = HomeFolder.INSTANCE.get();
		Path base = null;
		boolean success = false;
		if (!relative.isEmpty()) {
			base = tinboHome.resolve(relative);
			success = Check.condition(Files.exists(base) && Files.isDirectory(base),
					String.format("Given relative root path %s to kanbin is invalid/does not exist.", base));
		} else if (!absolute.isEmpty()) {
			base = Paths.get(absolute);
			success = Check.condition(Files.exists(base) && Files.isDirectory(base),
					String.format("Given absolute root path %s to kanbin is invalid/does not exist.", base));
		}
		if (!success) {
			base = HomeFolder.INSTANCE.getDirectory(Constants.KANBIN);
			String message = "kanbin: Could not determine root path. Falling back to %s.";
			PrintersKt.printlnInfo(String.format(message, base));
		}
		return base;
	}
}
