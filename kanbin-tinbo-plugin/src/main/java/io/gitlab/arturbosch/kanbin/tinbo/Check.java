package io.gitlab.arturbosch.kanbin.tinbo;

import io.gitlab.arturbosch.tinbo.api.utils.PrintersKt;

/**
 * @author Artur Bosch
 */
public final class Check {

	private Check() {
	}

	public static boolean condition(boolean condition, String message) {
		if (!condition) {
			PrintersKt.printlnInfo(message);
			return false;
		}
		return true;
	}
}
