package io.gitlab.arturbosch.kanbin.tinbo;

import io.gitlab.arturbosch.tinbo.api.config.ModeManager;
import io.gitlab.arturbosch.tinbo.api.config.TinboMode;
import io.gitlab.arturbosch.tinbo.api.marker.Command;
import io.gitlab.arturbosch.tinbo.api.utils.PrintersKt;
import org.jetbrains.annotations.NotNull;
import org.springframework.shell.core.annotation.CliAvailabilityIndicator;
import org.springframework.shell.core.annotation.CliCommand;
import org.springframework.stereotype.Component;

/**
 * @author Artur Bosch
 */
@Component
public class StartKanbinCommand implements Command {

	@NotNull
	@Override
	public String getId() {
		return "start";
	}

	@CliAvailabilityIndicator("kanbin")
	public boolean onlyStartMode() {
		return ModeManager.INSTANCE.isCurrentMode(TinboMode.Companion.getSTART());
	}

	@CliCommand(value = "kanbin", help = "Switch to kanbin mode to manage tasks kanban style.")
	public void kanbinMode() {
		ModeManager.INSTANCE.setCurrent(KanbinMode.getInstance());
		PrintersKt.printlnInfo("Entering kanbin mode...");
	}
}
