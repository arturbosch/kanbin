package io.gitlab.arturbosch.kanbin.tinbo;

import io.gitlab.arturbosch.kanbin.utils.Try;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.function.Predicate;
import java.util.regex.Pattern;

/**
 * @author Artur Bosch
 */
public class LaneCleaner implements FileVisitor<Path> {

	public static final Predicate<String> UUID_PATTERN = Pattern.compile(
			"[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}"
	).asPredicate();


	public static void removeCards(final Path root) {
		Try.just(() -> Files.walkFileTree(root, new LaneCleaner()));
	}

	@Override
	public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) {
		return FileVisitResult.CONTINUE;
	}

	@Override
	public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {
		String fileName = file.getFileName().toString();
		if (UUID_PATTERN.test(fileName)) {
			Try.just(() -> Files.deleteIfExists(file));
		}
		return FileVisitResult.CONTINUE;
	}

	@Override
	public FileVisitResult visitFileFailed(Path file, IOException exc) {
		return FileVisitResult.CONTINUE;
	}

	@Override
	public FileVisitResult postVisitDirectory(Path dir, IOException exc) {
		return FileVisitResult.CONTINUE;
	}
}
