package io.gitlab.arturbosch.kanbin.tinbo;

import io.gitlab.arturbosch.kanbin.models.Lane;
import io.gitlab.arturbosch.kanbin.models.Project;
import io.gitlab.arturbosch.tinbo.api.model.util.CSVTablePrinter;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Artur Bosch
 */
public class PrintBoardTable {

	private static final CSVTablePrinter csv = new CSVTablePrinter();
	private Project project;

	public PrintBoardTable(Project project) {
		this.project = project;
	}

	@Override
	public String toString() {
		return lanesToColumns()
				.map(csv::asTableString)
				.orElse("Not enough data.");
	}

	private Optional<List<String>> lanesToColumns() {
		List<List<String>> columns = project.getLanes().stream()
				.map(this::laneToColumn)
				.collect(Collectors.toList());

		List<String> rows = columnsToRows(columns);
		return Optional.ofNullable(rows);
	}

	private List<String> columnsToRows(List<List<String>> columns) {
		int maxRows = columns.stream().mapToInt(List::size).max().orElse(0);
		List<String> result = new ArrayList<>();
		for (int i = 0; i < maxRows; i++) {
			List<String> rowEntries = new ArrayList<>();
			for (List<String> column : columns) {
				String entry = i <= column.size() - 1 ? column.get(i) : " ";
				rowEntries.add(entry);
			}
			String row = rowEntries.stream().collect(Collectors.joining(";"));
			result.add(row);
		}
		return result;
	}

	private List<String> laneToColumn(Lane lane) {
		List<String> result = new ArrayList<>();
		lane.getCards().forEach(card -> result.add(card.getTitle()));
		result.add(0, lane.getName());
		return result;
	}
}
