package io.gitlab.arturbosch.kanbin.tinbo;

import io.gitlab.arturbosch.kanbin.ProjectChooser;
import io.gitlab.arturbosch.kanbin.ProjectExplorer;
import io.gitlab.arturbosch.kanbin.VirtualProject;
import io.gitlab.arturbosch.kanbin.formats.DataFormat;
import io.gitlab.arturbosch.kanbin.models.Card;
import io.gitlab.arturbosch.kanbin.models.Lane;
import io.gitlab.arturbosch.kanbin.utils.Pre;
import io.gitlab.arturbosch.kanbin.utils.Try;
import io.gitlab.arturbosch.tinbo.api.TinboBus;
import io.gitlab.arturbosch.tinbo.api.utils.PrintersKt;
import org.jetbrains.annotations.NotNull;

import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * @author Artur Bosch
 */
public class KanbinService {

	private final ProjectChooser chooser;
	private final ProjectExplorer explorer;

	private VirtualProject current = null;

	public KanbinService(@NotNull Path base, @NotNull DataFormat format) {
		chooser = new ProjectChooser(base);
		explorer = new ProjectExplorer(base, format);
	}

	public void init(@NotNull String lastProject) {
		Tasks.run(() -> chooser.find(lastProject)
				.ifPresent(name -> current = explorer.load(name)))
				.thenRun(this::publishLastUsedEvent);

	}

	private void publishLastUsedEvent() {
		if (current != null) {
			TinboBus.INSTANCE.publish(new LastUsedEvent(current.getProject().getName()));
		}
	}

	public String showAvailableProjects() {
		String content = chooser.getProjects().stream()
				.collect(Collectors.joining(", "));
		return content.isEmpty() ? "No projects available" : content;
	}

	public String showCurrentProject() {
		return Optional.ofNullable(current)
				.map(VirtualProject::getProject)
				.map(PrintBoardTable::new)
				.map(Object::toString)
				.orElse("No current project loaded.");
	}

	public void createProject(@NotNull String name, @NotNull Set<String> lanes) {
		Tasks.run(() -> {
			current = explorer.create(name, lanes);
			String newProject = current.getProject().getName();
			chooser.add(newProject);
			PrintersKt.printlnInfo(String.format("Created new project board with name %s", newProject));
		}).thenRun(this::publishLastUsedEvent);
	}

	public void loadProject(@NotNull String name) {
		Tasks.run(() -> {
			Optional<String> project = chooser.find(name);
			project.ifPresent(s -> current = explorer.load(s));
			if (!project.isPresent()) {
				PrintersKt.printlnInfo(String.format("There was no project board with name %s", name));
			} else {
				PrintersKt.printlnInfo(String.format("Loaded project board with name %s", name));
			}
		}).thenRun(this::publishLastUsedEvent);
	}

	public void moveProject(@NotNull String name) {
		assertCurrentVirtualProjectPresent();
		Tasks.run(() -> {
			String oldName = current.getProject().getName();
			current = explorer.move(current, name);
			chooser.migrate(oldName, name);
			PrintersKt.printlnInfo(String.format("Renamed and moved project board '%s' to '%s'", oldName, name));
		}).thenRun(this::publishLastUsedEvent);
	}

	public CompletableFuture<Void> withProject(@NotNull Consumer<VirtualProject> consumer) {
		assertCurrentVirtualProjectPresent();
		return Tasks.run(() -> consumer.accept(current));
	}

	private void assertCurrentVirtualProjectPresent() {
		Pre.condition(current != null, () -> "First create or load a project board to move it!");
	}

	public CompletableFuture<Void> findAndUpdateCard(@NotNull String title, @NotNull Card newCard) {
		assertCurrentVirtualProjectPresent();
		return Tasks.run(() -> {
			Card oldCard = findCardByTitle(title);
			Card newContentCard = oldCard.newCardWithNewContentDefaultToOldIfEmpty(newCard);
			current.updateCard(oldCard, newContentCard);
		});
	}

	@NotNull
	private Card findCardByTitle(@NotNull String title) {
		return current.getProject().getLanes().stream()
				.flatMap(lane -> lane.getCards().stream())
				.filter(card -> card.getTitle().startsWith(title))
				.findFirst()
				.orElseThrow(() ->
						new IllegalArgumentException("There is no card which title starts with '" + title + "'."));
	}

	public CompletableFuture<Void> findAndRemoveCard(@NotNull String title) {
		assertCurrentVirtualProjectPresent();
		return Tasks.run(() -> {
			Card card = findCardByTitle(title);
			current.deleteCard(card);
		});
	}

	public CompletableFuture<Void> findAndFinishCard(String title) {
		assertCurrentVirtualProjectPresent();
		return Tasks.run(() -> {
			Card card = findCardByTitle(title);
			List<Lane> lanes = current.getProject().getLanes();
			String lastLane = Util.lastElement(lanes).getName();
			Card update = card.newCardWithNewContentDefaultToOldIfEmpty(new Card("", "", lastLane));
			current.updateCard(card, update);
		});
	}

	public String existingLaneOrDefault(String lane) {
		assertCurrentVirtualProjectPresent();
		return current.getProject().findByName(lane)
				.map(Lane::getName)
				.orElse(current.getProject().getLanes().get(0).getName());
	}

	public void archiveLastLane() {
		assertCurrentVirtualProjectPresent();
		current.getProject().getLanes().stream()
				.max(Comparator.comparingInt(Lane::priority))
				.ifPresent(this::archiveCards);
	}

	public void archiveLane(String name) {
		assertCurrentVirtualProjectPresent();
		current.getProject().getLanes().stream()
				.filter(lane -> lane.getName().equals(name))
				.findFirst()
				.ifPresent(this::archiveCards);
	}

	private void archiveCards(Lane lane) {
		Path path = current.getPath().resolve(lane.getName());
		Path archive = current.getPath().resolve("." + lane.getName() + LocalDateTime.now());
		Try.just(() -> Files.createDirectories(archive));
		Try.just(() -> Files.walk(path))
				.filter(thePath -> !thePath.equals(path))
				.forEach(thePath -> Try.just(() -> Files.copy(thePath, archive.resolve(thePath.getFileName()))));
		LaneCleaner.removeCards(path);
		lane.clean();
	}
}
