package io.gitlab.arturbosch.kanbin.tinbo;

import io.gitlab.arturbosch.tinbo.api.config.TinboMode;
import org.jetbrains.annotations.NotNull;

/**
 * @author Artur Bosch
 */
public class KanbinMode implements TinboMode {

	public static KanbinMode getInstance() {
		return Holder.INSTANCE;
	}

	private static class Holder {
		static final KanbinMode INSTANCE = new KanbinMode();

		private Holder() {
		}
	}

	@NotNull
	@Override
	public String getId() {
		return Constants.KANBIN;
	}

	@NotNull
	@Override
	public String[] getHelpIds() {
		return new String[]{getId(), "share", "mode"};
	}

	@Override
	public boolean getEditAllowed() {
		return true;
	}

	@Override
	public boolean isSummarizable() {
		return false;
	}
}
