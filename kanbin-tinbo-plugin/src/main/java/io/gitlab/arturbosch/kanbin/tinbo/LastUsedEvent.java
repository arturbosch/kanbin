package io.gitlab.arturbosch.kanbin.tinbo;

/**
 * @author Artur Bosch
 */
public class LastUsedEvent {

	private final String board;

	public LastUsedEvent(final String board) {
		this.board = board;
	}

	public String boardName() {
		return board;
	}
}
