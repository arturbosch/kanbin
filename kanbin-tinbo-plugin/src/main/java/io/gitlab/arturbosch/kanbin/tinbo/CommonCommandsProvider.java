package io.gitlab.arturbosch.kanbin.tinbo;

import io.gitlab.arturbosch.kanbin.models.Card;
import io.gitlab.arturbosch.kanbin.utils.Pre;
import io.gitlab.arturbosch.kanbin.utils.Try;
import io.gitlab.arturbosch.tinbo.api.TinboTerminal;
import io.gitlab.arturbosch.tinbo.api.marker.Addable;
import io.gitlab.arturbosch.tinbo.api.marker.Command;
import io.gitlab.arturbosch.tinbo.api.marker.Datable;
import io.gitlab.arturbosch.tinbo.api.marker.Listable;
import io.gitlab.arturbosch.tinbo.api.marker.Loadable;
import org.jetbrains.annotations.NotNull;
import org.springframework.shell.standard.commands.Clear;
import org.springframework.stereotype.Component;

/**
 * @author Artur Bosch
 */
@Component
public class CommonCommandsProvider implements Command, Addable, Listable, Datable, Loadable {

	private final KanbinService service;
	private final TinboTerminal console;
	private final Clear clearCommand;

	public CommonCommandsProvider(KanbinService service,
								  TinboTerminal console,
								  Clear clearCommand) {
		this.service = service;
		this.console = console;
		this.clearCommand = clearCommand;
	}

	@NotNull
	@Override
	public String getId() {
		return Constants.KANBIN;
	}

	@NotNull
	@Override
	public String add() {
		String title = Pre.notEmpty(Try.just(() -> console.readLine("Enter a title: ")));
		String lane = Pre.notNull(Try.just(() -> console.readLine("Enter a lane: ")));
		String text = Pre.notNull(Try.just(() -> console.readLine("Enter a description: ")));
		String givenOrFirst = service.existingLaneOrDefault(lane);
		service.withProject(project -> project.generateCard(givenOrFirst, new Card(title, text, givenOrFirst)))
				.thenRun(this::clear)
				.thenRun(() -> console.write(list()));
		return "";
	}

	public String list() {
		return list("", true);
	}

	@NotNull
	@Override
	public String list(@NotNull String category, boolean all) {
		return service.showCurrentProject();
	}

	@NotNull
	@Override
	public String data() {
		return service.showAvailableProjects();
	}

	@NotNull
	@Override
	public String load(@NotNull String projectName) {
		service.loadProject(projectName);
		return "";
	}

	public void clear() {
		clearCommand.clear();
	}
}
