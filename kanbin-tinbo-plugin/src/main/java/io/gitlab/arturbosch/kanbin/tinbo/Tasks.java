package io.gitlab.arturbosch.kanbin.tinbo;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ForkJoinPool;

/**
 * @author Artur Bosch
 */
public final class Tasks {

	private Tasks() {
	}

	private static final ExecutorService EXECUTOR = ForkJoinPool.commonPool();

	public static CompletableFuture<Void> run(Runnable task) {
		return CompletableFuture.runAsync(task, EXECUTOR);
	}
}
