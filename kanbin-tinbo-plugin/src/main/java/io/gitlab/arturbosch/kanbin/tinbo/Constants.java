package io.gitlab.arturbosch.kanbin.tinbo;

/**
 * @author Artur Bosch
 */
public final class Constants {

	private Constants() {
	}

	public static final String KANBIN = "kanbin";
}
