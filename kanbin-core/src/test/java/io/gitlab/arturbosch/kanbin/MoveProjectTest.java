package io.gitlab.arturbosch.kanbin;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static io.gitlab.arturbosch.kanbin.Util.TEST;

/**
 * @author Artur Bosch
 */
public class MoveProjectTest {

	@Test
	void createAndMoveProject() throws IOException {
		Path tmp = Files.createTempDirectory("kanbin_move_projects");
		ProjectExplorer explorer = new ProjectExplorer(tmp, new TestFormat());

		VirtualProject oldProject = explorer.create(TEST, Util.setOf("A", "B", "C"));
		VirtualProject movedProject = explorer.move(oldProject, "MovedTest");

		Assertions.assertTrue(Files.exists(movedProject.getPath()));
		Assertions.assertTrue(Files.notExists(oldProject.getPath()));
	}
}
