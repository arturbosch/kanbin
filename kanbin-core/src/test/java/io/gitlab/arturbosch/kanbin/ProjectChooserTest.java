package io.gitlab.arturbosch.kanbin;

import io.gitlab.arturbosch.kanbin.utils.Resources;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author Artur Bosch
 */
class ProjectChooserTest {

	@Test
	void getAllProjects() {
		ProjectChooser chooser = new ProjectChooser(Resources.asPath("projects/Test"));

		Set<String> projects = chooser.getProjects();

		assertTrue(projects.contains("A"));
		assertTrue(projects.contains("B"));
		assertTrue(projects.contains("C"));
	}
}
