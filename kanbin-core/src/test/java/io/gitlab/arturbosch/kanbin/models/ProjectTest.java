package io.gitlab.arturbosch.kanbin.models;

import io.gitlab.arturbosch.kanbin.Util;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * @author Artur Bosch
 */
class ProjectTest {

	@Test
	void containsLane() {
		Project project = new Project("Test",
				Util.setOf(Lane.of("A", 1), Lane.of("B", 2)));

		Assertions.assertTrue(project.containsByName("A"));
		Assertions.assertTrue(project.containsByName("B"));
	}

	@Test
	void containsLaneAfterRenaming() {
		Project project = new Project("Test",
				Util.setOf(Lane.of("A", 1), Lane.of("B", 2)));

		project.findByName("A")
				.ifPresent(lane -> lane.updateName("C"));

		Assertions.assertFalse(project.containsByName("A"));
		Assertions.assertTrue(project.containsByName("B"));
		Assertions.assertTrue(project.containsByName("C"));

	}
}
