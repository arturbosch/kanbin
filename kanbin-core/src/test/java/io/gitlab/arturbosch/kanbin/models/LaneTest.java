package io.gitlab.arturbosch.kanbin.models;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * @author Artur Bosch
 */
class LaneTest {

	@Test
	void renamingLaneResultsInUpdatingCards() {
		Lane lane = new Lane("Test", new LaneSettings());
		lane.addCard(new Card("1", "1", "Test"));
		lane.addCard(new Card("2", "2", "Test"));

		lane.updateName("Done");

		Assertions.assertTrue(lane.getCards().stream().allMatch(card -> card.getLane().equals("Done")));
	}
}
