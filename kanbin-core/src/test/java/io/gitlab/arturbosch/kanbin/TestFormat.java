package io.gitlab.arturbosch.kanbin;

import io.gitlab.arturbosch.kanbin.formats.DataFormat;
import io.gitlab.arturbosch.kanbin.models.Card;
import io.gitlab.arturbosch.kanbin.models.Lane;
import io.gitlab.arturbosch.kanbin.models.LaneSettings;
import io.gitlab.arturbosch.kanbin.models.Project;
import org.apache.commons.lang3.StringUtils;

import java.nio.file.Path;

import static io.gitlab.arturbosch.kanbin.Util.TEST;

/**
 * @author Artur Bosch
 */
class TestFormat implements DataFormat {

	@Override
	public Project fromFile(Path path) {
		return new Project(TEST, Util.setOf(
				lane("A", 1), lane("B", 2), lane("C", 3)));
	}

	@Override
	public String toString(Card card) {
		return card.toString();
	}

	@Override
	public String toString(Lane lane) {
		return lane.toString();
	}

	@Override
	public Card fromString(String card) {
		String beforeUuid = StringUtils.substringAfter(card, "uuid='");
		String uuid = StringUtils.substringBefore(beforeUuid, "'");
		String beforeTitle = StringUtils.substringAfter(beforeUuid, "title='");
		String title = StringUtils.substringBefore(beforeTitle, "'");
		String beforeText = StringUtils.substringAfter(beforeUuid, "text='");
		String text = StringUtils.substringBefore(beforeText, "'");
		String beforeLane = StringUtils.substringAfter(beforeUuid, "lane='");
		String lane = StringUtils.substringBefore(beforeLane, "'");
		return new Card(uuid, title, text, lane);
	}

	private Lane lane(String name, int prio) {
		return new Lane(name, new LaneSettings(prio));
	}
}
