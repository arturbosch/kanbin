package io.gitlab.arturbosch.kanbin;

import io.gitlab.arturbosch.kanbin.models.Project;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static io.gitlab.arturbosch.kanbin.Util.TEST;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author Artur Bosch
 */
class CreateProjectTest {

	@Test
	void createProjectWithThreeLanes() throws IOException {
		Path tmp = Files.createTempDirectory("kanbin_test_projects");
		ProjectExplorer explorer = new ProjectExplorer(tmp, new TestFormat());

		Project project = explorer.create(TEST, Util.setOf("A", "B", "C")).getProject();
		Path projectPath = tmp.resolve(project.getName());

		assertEquals(project.getName(), TEST);
		assertEquals(project.getLanes().size(), 3);
		assertTrue(Files.exists(projectPath.resolve("A")));
		assertTrue(Files.exists(projectPath.resolve("B")));
		assertTrue(Files.exists(projectPath.resolve("C")));
	}
}
