package io.gitlab.arturbosch.kanbin;

import io.gitlab.arturbosch.kanbin.models.Project;
import io.gitlab.arturbosch.kanbin.utils.Resources;
import org.junit.jupiter.api.Test;

import java.nio.file.Path;

import static io.gitlab.arturbosch.kanbin.Util.TEST;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author Artur Bosch
 */
public class LoadProjectTest {

	@Test
	void loadProjectWithThreeLanes() {
		Path resource = Resources.asPath("projects");
		ProjectExplorer explorer = new ProjectExplorer(resource, new TestFormat());

		Project project = explorer.load(TEST).getProject();

		assertEquals(project.getName(), TEST);
		assertEquals(project.getLanes().size(), 3);
	}
}
