package io.gitlab.arturbosch.kanbin;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Artur Bosch
 */
public final class Util {

	private Util() {
	}

	public static final String TEST = "Test";

	@SafeVarargs
	public static <T> Set<T> setOf(T... elements) {
		return new HashSet<>(Arrays.asList(elements));
	}
}
