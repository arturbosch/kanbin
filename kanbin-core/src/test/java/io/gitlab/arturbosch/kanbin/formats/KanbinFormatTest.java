package io.gitlab.arturbosch.kanbin.formats;

import io.gitlab.arturbosch.kanbin.ProjectExplorer;
import io.gitlab.arturbosch.kanbin.Util;
import io.gitlab.arturbosch.kanbin.VirtualProject;
import io.gitlab.arturbosch.kanbin.models.Card;
import io.gitlab.arturbosch.kanbin.models.Project;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static io.gitlab.arturbosch.kanbin.Util.TEST;

/**
 * @author Artur Bosch
 */
@SuppressWarnings("ConstantConditions")
class KanbinFormatTest {

	@Test
	void convertCardFromAndToYaml() {
		KanbinFormat format = new KanbinFormat();
		Card card = new Card("Hello", "World", "A");

		String yaml = format.toString(card);
		Card restoredCard = format.fromString(yaml);

		Assertions.assertEquals(card, restoredCard);
	}

	@Test
	void loadProject() throws IOException {
		// given: project with three lanes and three cards
		Path tmp = Files.createTempDirectory("kanbin_formats");
		KanbinFormat format = new KanbinFormat();
		ProjectExplorer explorer = new ProjectExplorer(tmp, format);
		VirtualProject project = explorer.create(TEST, Util.setOf("A", "B", "C"));
		project.generateCard("A", new Card("1", "", "A"));
		project.generateCard("A", new Card("2", "", "A"));
		project.generateCard("B", new Card("1", "", "B"));

		// when: loading the project
		Path projectPath = project.getPath();
		Project loadedProject = format.fromFile(projectPath);

		// then: three lanes and cards are present
		Assertions.assertEquals(loadedProject.getLanes().size(), 3);
		Assertions.assertEquals(loadedProject.findByName("A").get().getCards().size(), 2);
		Assertions.assertEquals(loadedProject.findByName("B").get().getCards().size(), 1);
	}
}
