package io.gitlab.arturbosch.kanbin;

import io.gitlab.arturbosch.kanbin.formats.DataFormat;
import io.gitlab.arturbosch.kanbin.models.Card;
import io.gitlab.arturbosch.kanbin.models.Lane;
import io.gitlab.arturbosch.kanbin.models.Project;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collection;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

import static io.gitlab.arturbosch.kanbin.Util.TEST;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author Artur Bosch
 */
class ProjectGenerationTest {

	private BiFunction<Collection<Lane>, String, Lane> laneChooser = (lanes, name) -> lanes.stream()
			.filter(lane -> lane.getName().equals(name))
			.findFirst()
			.orElseThrow(() -> new RuntimeException("Unexpected lane name."));

	private static final DataFormat format = new TestFormat();

	@Test
	void expectPathsAreCreatedForLanes() throws IOException {
		Path tmp = Files.createTempDirectory("kanbin_test_projects");
		VirtualProject generation = new VirtualProject(tmp, new Project(TEST), format).generate();

		generation.generateLane("A");
		generation.generateLane("B");
		generation.generateLane("C");
		Project project = generation.getProject();
		Collection<Lane> lanes = project.getLanes();
		Path projectPath = tmp.resolve(project.getName());

		Lane laneD = generation.generateLane("D", 1);
		Lane laneE = generation.generateLane("E", 0);
		Lane laneF = generation.generateLane("F", project.getLanes().size());
		Lane laneG = generation.generateLane("G", project.getLanes().size() - 1);

		assertTrue(Files.exists(projectPath));
		assertAll(() -> {
			assertTrue(Files.exists(projectPath.resolve("A")));
			assertTrue(Files.exists(projectPath.resolve("B")));
			assertTrue(Files.exists(projectPath.resolve("C")));
			assertTrue(Files.exists(projectPath.resolve("D")));
			assertTrue(Files.exists(projectPath.resolve("E")));
			assertTrue(Files.exists(projectPath.resolve("F")));
			assertTrue(Files.exists(projectPath.resolve("G")));
		});

		// Order: E, A, D, B, C, G, F
		assertAll(() -> {
			assertTrue(laneChooser.apply(lanes, "A").priority() == 1);
			assertTrue(laneChooser.apply(lanes, "B").priority() == 3);
			assertTrue(laneChooser.apply(lanes, "C").priority() == 4);
			assertTrue(laneD.priority() == 2);
			assertTrue(laneE.priority() == 0);
			assertTrue(laneF.priority() == 6);
			assertTrue(laneG.priority() == 5);
		});
	}

	@Test
	void removeLanes() throws IOException {
		Path tmp = Files.createTempDirectory("kanbin_move_projects");
		VirtualProject generation = new VirtualProject(tmp, new Project(TEST), format).generate();
		Path path = generation.getPath();

		generation.generateLane("A");
		generation.generateLane("B");
		generation.generateLane("C");
		generation.removeLane("A", null);

		assertThrows(IllegalArgumentException.class, () -> generation.removeLane("B", "D"));
		assertThrows(IllegalArgumentException.class, () -> generation.removeLane("B", null));
		assertThrows(IllegalArgumentException.class, () -> generation.removeLane("C", "A"));

		Assertions.assertTrue(Files.notExists(path.resolve("A")));
		Assertions.assertTrue(Files.exists(path.resolve("B")));
		Assertions.assertTrue(Files.exists(path.resolve("C")));
	}

	@Test
	void moveLaneFromAToB() throws IOException {
		Path tmp = Files.createTempDirectory("kanbin_move_projects");
		VirtualProject generation = new VirtualProject(tmp, new Project(TEST), format).generate();
		Path path = generation.getPath();

		generation.generateLane("A");

		Lane lane = generation.updateLaneName("B", "A");
		Assertions.assertTrue(lane.getName().equals("B"));
		Assertions.assertTrue(Files.exists(path.resolve("B")));
		Assertions.assertTrue(Files.notExists(path.resolve("A")));
	}

	@Test
	void createMoveUpdateDeleteCard() throws IOException {
		// given: a project
		Path tmp = Files.createTempDirectory("kanbin_create_cards");
		VirtualProject generation = new VirtualProject(tmp, new Project(TEST), format).generate();
		Path path = generation.getPath();

		// when: generating a card with name 'Hello'
		generation.generateLane("A");
		Card card = new Card("Hello", "World", "A");
		TestFormat format = new TestFormat();
		generation.generateCard("A", card);

		// then: path to card must exist
		Path cardPath = path.resolve("A").resolve(card.getUuid());
		Assertions.assertTrue(Files.exists(cardPath));

		// when: loading the card
		String content = Files.lines(cardPath).collect(Collectors.joining("n"));
		Card loadedCard = format.fromString(content);

		// then: it must be equals to saved one
		Assertions.assertEquals(card, loadedCard);

		// when: updating the card
		Assertions.assertThrows(IllegalArgumentException.class,
				() -> generation.updateCard(card, new Card("World", "Hello", "B")));
		generation.updateCard(card, new Card("World", "Hello", "A"));

		// then: the card was updated
		Assertions.assertEquals(card.getTitle(), "World");
		Assertions.assertEquals(card.getText(), "Hello");
		Assertions.assertTrue(Files.exists(cardPath));
		Assertions.assertTrue(Files.notExists(path.resolve("B").resolve(card.getUuid())));

		// when: deleting the card
		generation.deleteCard(card);

		// then: path and card are deleted from lane
		Assertions.assertTrue(Files.exists(path.resolve("A")));
		Assertions.assertTrue(Files.notExists(cardPath));
	}
}
