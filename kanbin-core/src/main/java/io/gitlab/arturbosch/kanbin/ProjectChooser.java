package io.gitlab.arturbosch.kanbin;

import io.gitlab.arturbosch.kanbin.utils.Pre;
import io.gitlab.arturbosch.kanbin.utils.Try;
import org.jetbrains.annotations.NotNull;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Artur Bosch
 */
public class ProjectChooser {

	private final Set<String> projects;

	public ProjectChooser(Path basePath) {
		this.projects = Try.just(() -> Files.walk(basePath, 1)
				.filter(path -> !path.equals(basePath))
				.map(path -> path.getFileName().toString())
				.collect(Collectors.toSet()));
	}

	public Set<String> getProjects() {
		return projects;
	}

	public Optional<String> find(@NotNull String name) {
		return getProjects().stream()
				.filter(project -> project.equals(name))
				.findFirst();
	}

	public void add(@NotNull String project) {
		Pre.notEmpty(project);
		projects.add(project);
	}

	public void migrate(@NotNull String oldName, @NotNull String newName) {
		Pre.notEmpty(oldName);
		Pre.notEmpty(newName);
		projects.remove(oldName);
		projects.add(newName);
	}
}
