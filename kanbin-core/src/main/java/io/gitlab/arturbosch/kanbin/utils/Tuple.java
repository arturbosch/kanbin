package io.gitlab.arturbosch.kanbin.utils;

/**
 * @author Artur Bosch
 */
public class Tuple<First, Second> {

	private final First first;
	private final Second second;

	private Tuple(First first, Second second) {
		this.first = first;
		this.second = second;
	}

	public static <First, Second> Tuple<First, Second> of(First first, Second second) {
		Pre.notNull(first);
		Pre.notNull(second);
		return new Tuple<>(first, second);
	}

	public First getFirst() {
		return first;
	}

	public Second getSecond() {
		return second;
	}

	@Override
	public String toString() {
		return "Tuple{" +
				"first=" + first +
				", second=" + second +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Tuple<?, ?> tuple = (Tuple<?, ?>) o;

		if (first != null ? !first.equals(tuple.first) : tuple.first != null) return false;
		return second != null ? second.equals(tuple.second) : tuple.second == null;
	}

	@Override
	public int hashCode() {
		int result = first != null ? first.hashCode() : 0;
		result = 31 * result + (second != null ? second.hashCode() : 0);
		return result;
	}
}
