package io.gitlab.arturbosch.kanbin.utils;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

/**
 * @author Artur Bosch
 */
public final class Dirs {

	private Dirs() {
	}

	public static void clear(Path dir) {
		Try.just(() ->
				Files.walkFileTree(dir, new SimpleFileVisitor<Path>() {
					@Override
					public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
						Files.deleteIfExists(file);
						return FileVisitResult.CONTINUE;
					}

					@Override
					public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
						if (exc == null) {
							Files.deleteIfExists(dir);
						} else {
							throw exc;
						}
						return FileVisitResult.CONTINUE;
					}
				})
		);
	}

	public static void move(Path from, Path to) {
		Try.just(() -> Files.move(from, to));
	}

	public static void write(Path path, String content) {
		Try.just(() -> Files.write(path, content.getBytes()));
	}
}
