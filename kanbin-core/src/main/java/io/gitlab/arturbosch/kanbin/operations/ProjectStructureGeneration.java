package io.gitlab.arturbosch.kanbin.operations;

import io.gitlab.arturbosch.kanbin.formats.DataFormat;
import io.gitlab.arturbosch.kanbin.models.Lane;
import io.gitlab.arturbosch.kanbin.models.Project;

import java.nio.file.Path;

/**
 * @author Artur Bosch
 */
public class ProjectStructureGeneration {

	private final Project project;
	private final LaneGeneration laneGeneration;

	public ProjectStructureGeneration(Project project, Path projectPath, DataFormat format) {
		this.project = project;
		this.laneGeneration = new LaneGeneration(project, projectPath, format);
	}

	public void generate() {
		for (Lane lane : project.getLanes()) {
			laneGeneration.createLaneFiles(lane);
		}
	}
}
