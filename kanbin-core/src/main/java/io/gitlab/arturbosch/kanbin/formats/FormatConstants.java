package io.gitlab.arturbosch.kanbin.formats;

import java.nio.file.Path;
import java.util.function.Predicate;

/**
 * @author Artur Bosch
 */
public final class FormatConstants {

	public static final String SETTINGS_FILE = ".settings";

	public static final Predicate<Path> NOT_SETTINGS_FILE =
			file -> !file.getFileName().toString().equals(SETTINGS_FILE);

	private FormatConstants() {
	}
}
