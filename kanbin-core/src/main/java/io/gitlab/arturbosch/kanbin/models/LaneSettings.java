package io.gitlab.arturbosch.kanbin.models;

/**
 * @author Artur Bosch
 */
public class LaneSettings {

	private int priority;

	LaneSettings() {
	}

	public LaneSettings(int priority) {
		this.priority = priority;
	}

	@Override
	public String toString() {
		return "Settings{" +
				"priority=" + priority +
				'}';
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}
}
