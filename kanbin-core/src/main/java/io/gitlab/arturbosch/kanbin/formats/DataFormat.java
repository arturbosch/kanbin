package io.gitlab.arturbosch.kanbin.formats;

import io.gitlab.arturbosch.kanbin.models.Card;
import io.gitlab.arturbosch.kanbin.models.Lane;
import io.gitlab.arturbosch.kanbin.models.Project;

import java.nio.file.Path;
import java.util.function.Predicate;

/**
 * @author Artur Bosch
 */
public interface DataFormat {

	Project fromFile(Path path);

	String toString(Card card);

	String toString(Lane lane);

	Card fromString(String card);
}
