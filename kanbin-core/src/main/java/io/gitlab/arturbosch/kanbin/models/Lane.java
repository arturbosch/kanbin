package io.gitlab.arturbosch.kanbin.models;

import io.gitlab.arturbosch.kanbin.utils.Pre;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Artur Bosch
 */
public class Lane {

	private String name;
	private LaneSettings settings;
	private Set<Card> cards;

	public Lane(String name, LaneSettings settings) {
		this.name = name;
		this.settings = settings;
		this.cards = new HashSet<>();
	}

	public Lane(String name, LaneSettings settings, Set<Card> cards) {
		this.name = name;
		this.settings = settings;
		this.cards = cards;
	}

	public static Lane of(String name, int index) {
		Pre.notEmpty(name);
		return new Lane(name, new LaneSettings(index));
	}

	public void move(int priority) {
		settings.setPriority(priority);
	}

	public void moveByOne() {
		move(priority() + 1);
	}

	public void updateName(String newName) {
		this.name = Pre.notEmpty(newName);
		this.cards.forEach(card -> card.updateLane(this));
	}

	public void addCard(Card card) {
		Pre.notNull(card);
		card.updateLane(this);
		cards.add(card);
	}

	public void removeCard(Card card) {
		Pre.notNull(card);
		cards.remove(card);
	}

	public void clean() {
		cards.clear();
	}

	@Override
	public String toString() {
		return "Lane{" +
				"name='" + name + '\'' +
				", settings=" + settings +
				", cards=" + cards +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Lane lane = (Lane) o;

		return name != null ? name.equals(lane.name) : lane.name == null;
	}

	@Override
	public int hashCode() {
		return name != null ? name.hashCode() : 0;
	}

	public String getName() {
		return name;
	}

	public LaneSettings getSettings() {
		return settings;
	}

	public int priority() {
		return getSettings().getPriority();
	}

	public Set<Card> getCards() {
		return Collections.unmodifiableSet(cards);
	}
}
