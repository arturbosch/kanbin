package io.gitlab.arturbosch.kanbin.operations;

import io.gitlab.arturbosch.kanbin.formats.DataFormat;
import io.gitlab.arturbosch.kanbin.models.Card;
import io.gitlab.arturbosch.kanbin.models.Lane;
import io.gitlab.arturbosch.kanbin.models.Project;
import io.gitlab.arturbosch.kanbin.utils.Dirs;
import io.gitlab.arturbosch.kanbin.utils.Pre;

import java.nio.file.Path;
import java.util.Optional;

/**
 * @author Artur Bosch
 */
public class LaneRenaming {

	private final Project project;
	private final Path projectPath;
	private final DataFormat format;

	public LaneRenaming(Project project, Path projectPath, DataFormat format) {
		this.project = project;
		this.projectPath = projectPath;
		this.format = format;
	}

	public Lane rename(String newName, String oldName) {
		Optional<Lane> maybeLane = project.findByName(oldName);
		Pre.state(maybeLane.isPresent(), () -> String.format("No lane with name '%s' available.", oldName));
		Lane theLane = maybeLane.get();
		Dirs.move(projectPath.resolve(oldName), projectPath.resolve(newName));
		theLane.updateName(newName);
		updatePersistedCards(theLane);
		return theLane;
	}

	private void updatePersistedCards(Lane lane) {
		for (Card card : lane.getCards()) {
			Path newPath = projectPath.resolve(lane.getName()).resolve(card.getUuid());
			Dirs.write(newPath, format.toString(card));
		}
	}
}
