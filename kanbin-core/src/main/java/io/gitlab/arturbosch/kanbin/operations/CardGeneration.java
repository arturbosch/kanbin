package io.gitlab.arturbosch.kanbin.operations;

import io.gitlab.arturbosch.kanbin.formats.DataFormat;
import io.gitlab.arturbosch.kanbin.models.Card;
import io.gitlab.arturbosch.kanbin.models.Project;
import io.gitlab.arturbosch.kanbin.utils.Dirs;
import io.gitlab.arturbosch.kanbin.utils.Pre;
import io.gitlab.arturbosch.kanbin.utils.Try;

import java.nio.file.Files;
import java.nio.file.Path;

/**
 * @author Artur Bosch
 */
public class CardGeneration {

	private final Project project;
	private final Path projectPath;
	private DataFormat format;

	public CardGeneration(Project project, Path projectPath, DataFormat format) {
		this.project = project;
		this.projectPath = projectPath;
		this.format = format;
	}

	public void create(String lane, Card card) {
		project.findByName(lane).ifPresent(theLane -> {
			theLane.addCard(card);
			String formattedCard = format.toString(card);
			Path cardPath = projectPath.resolve(lane).resolve(card.getUuid());
			Try.to(() -> Files.write(cardPath, formattedCard.getBytes()));
		});
	}

	public void update(Card oldCard, Card newCard) {
		String newLane = newCard.getLane();
		String uuid = oldCard.getUuid();
		Path oldPath = projectPath.resolve(oldCard.getLane()).resolve(uuid);
		Path newPath = projectPath.resolve(newLane).resolve(uuid);
		checkDifferentLanesSameUuidState(oldCard, newLane, uuid, newPath);
		updateLaneModels(oldCard, newLane);
		Dirs.move(oldPath, newPath);
		oldCard.updateCard(newCard.getTitle(), newCard.getText(), newLane);
		Dirs.write(newPath, format.toString(oldCard));
	}

	private void checkDifferentLanesSameUuidState(Card oldCard, String newLane, String uuid, Path newPath) {
		if (!oldCard.getLane().equals(newLane)) {
			Pre.state(Files.notExists(newPath),
					() -> String.format("There is already the uuid '%s' in new lane '%s'", uuid, newLane));
		}
	}

	private void updateLaneModels(Card oldCard, String newLane) {
		String oldLane = oldCard.getLane();
		if (!oldLane.equals(newLane)) {
			project.findByName(oldLane).ifPresent(lane -> lane.removeCard(oldCard));
			project.findByName(newLane).ifPresent(lane -> lane.addCard(oldCard));
		}
	}

	public void delete(Card card) {
		Path oldPath = projectPath.resolve(card.getLane()).resolve(card.getUuid());
		Try.to(() -> Files.deleteIfExists(oldPath));
		project.findByName(card.getLane()).ifPresent(lane -> lane.removeCard(card));
	}
}

