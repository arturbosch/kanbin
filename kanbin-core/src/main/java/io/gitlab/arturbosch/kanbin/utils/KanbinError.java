package io.gitlab.arturbosch.kanbin.utils;

/**
 * @author Artur Bosch
 */
public class KanbinError extends RuntimeException {

	public KanbinError(Throwable cause) {
		super(cause);
	}
}
