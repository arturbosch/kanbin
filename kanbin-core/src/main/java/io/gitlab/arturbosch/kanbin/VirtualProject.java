package io.gitlab.arturbosch.kanbin;

import io.gitlab.arturbosch.kanbin.formats.DataFormat;
import io.gitlab.arturbosch.kanbin.models.Card;
import io.gitlab.arturbosch.kanbin.models.Lane;
import io.gitlab.arturbosch.kanbin.models.Project;
import io.gitlab.arturbosch.kanbin.operations.CardGeneration;
import io.gitlab.arturbosch.kanbin.operations.LaneDeletion;
import io.gitlab.arturbosch.kanbin.operations.LaneGeneration;
import io.gitlab.arturbosch.kanbin.operations.LaneRenaming;
import io.gitlab.arturbosch.kanbin.operations.ProjectStructureGeneration;
import io.gitlab.arturbosch.kanbin.utils.Pre;
import io.gitlab.arturbosch.kanbin.utils.Try;
import org.jetbrains.annotations.Nullable;

import java.nio.file.Files;
import java.nio.file.Path;

/**
 * @author Artur Bosch
 */
public class VirtualProject {

	public static final String THERE_IS_NO_LANE_WITH_NAME_S_TO_ADD_CARDS = "There is no lane with name '%s' to add cards.";
	private final Project project;
	private final Path projectPath;
	private final DataFormat format;

	public VirtualProject(Path basePath, Project project, DataFormat format) {
		this.format = format;
		Pre.notNull(basePath, project);
		Pre.condition(Files.exists(basePath), () -> "Given base path must exist.");
		Path path = basePath.resolve(project.getName());
		this.projectPath = Try.to(() -> Files.createDirectories(path)).get();
		this.project = project;
	}

	VirtualProject generate() {
		new ProjectStructureGeneration(project, projectPath, format).generate();
		return this;
	}

	public Lane generateLane(String name) {
		return generateLane(name, project.getLanes().size());
	}

	public Lane generateLane(String name, int index) {
		Pre.notEmpty(name);
		return new LaneGeneration(project, projectPath, format).generate(name, index);
	}

	public VirtualProject removeLane(String laneToRemove, @Nullable String laneToMoveCards) {
		Pre.notEmpty(laneToRemove);
		Pre.condition(project.getLanes().size() > 2, () -> "A lane cannot be removed if only two exist.");
		new LaneDeletion(project, projectPath).remove(laneToRemove, laneToMoveCards);
		return this;
	}

	public Lane updateLaneName(String newName, String oldName) {
		Pre.notEmpty(newName, oldName);
		Pre.condition(Files.notExists(projectPath.resolve(newName)),
				() -> String.format("There is already a lane with name '%s'.", newName));
		return new LaneRenaming(project, projectPath, format).rename(newName, oldName);
	}

	public void generateCard(String lane, Card card) {
		Pre.notNull(card, format);
		Pre.notEmpty(lane);
		Pre.condition(project.containsByName(lane),
				() -> String.format(THERE_IS_NO_LANE_WITH_NAME_S_TO_ADD_CARDS, lane));
		Pre.condition(Files.notExists(projectPath.resolve(lane).resolve(card.getUuid())),
				() -> String.format("There is already a card with uuid '%s'.", card.getUuid()));
		new CardGeneration(project, projectPath, format).create(lane, card);
	}

	public void updateCard(Card oldCard, Card newCard) {
		Pre.notNull(oldCard, newCard);
		Pre.condition(project.containsByName(oldCard.getLane()),
				() -> String.format(THERE_IS_NO_LANE_WITH_NAME_S_TO_ADD_CARDS, oldCard.getLane()));
		Pre.condition(project.containsByName(newCard.getLane()),
				() -> String.format(THERE_IS_NO_LANE_WITH_NAME_S_TO_ADD_CARDS, newCard.getLane()));
		new CardGeneration(project, projectPath, format).update(oldCard, newCard);
	}

	public void deleteCard(Card card) {
		Pre.notNull(card);
		Pre.condition(project.containsByName(card.getLane()),
				() -> String.format("No lane with name '%s' found to delete card with uuid '%s'",
						card.getLane(), card.getUuid()));
		new CardGeneration(project, projectPath, format).delete(card);
	}

	public Path getPath() {
		return projectPath;
	}

	public Project getProject() {
		return project;
	}
}
