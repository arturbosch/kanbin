package io.gitlab.arturbosch.kanbin.operations;

import io.gitlab.arturbosch.kanbin.utils.Dirs;
import io.gitlab.arturbosch.kanbin.models.Lane;
import io.gitlab.arturbosch.kanbin.utils.Pre;
import io.gitlab.arturbosch.kanbin.models.Project;
import org.jetbrains.annotations.Nullable;

import java.nio.file.Path;
import java.util.Optional;

/**
 * @author Artur Bosch
 */
public class LaneDeletion {

	private final Project project;
	private final Path projectPath;

	public LaneDeletion(Project project, Path projectPath) {
		this.project = project;
		this.projectPath = projectPath;
	}

	public void remove(String toRemove, @Nullable String toMove) {
		project.findByName(toRemove).ifPresent(lane -> {
			if (Pre.isEmpty(toMove)) {
				project.removeLane(lane);
			} else {
				Optional<Lane> laneToMove = project.findByName(toMove);
				Pre.condition(laneToMove.isPresent(), () -> "Lane to move cards to is not present.");
				project.removeLane(lane, laneToMove.get());
			}
			Dirs.clear(projectPath.resolve(toRemove));
		});
	}
}
