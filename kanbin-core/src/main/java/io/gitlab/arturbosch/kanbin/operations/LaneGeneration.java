package io.gitlab.arturbosch.kanbin.operations;

import io.gitlab.arturbosch.kanbin.formats.DataFormat;
import io.gitlab.arturbosch.kanbin.formats.FormatConstants;
import io.gitlab.arturbosch.kanbin.models.Lane;
import io.gitlab.arturbosch.kanbin.models.Project;
import io.gitlab.arturbosch.kanbin.utils.Dirs;
import io.gitlab.arturbosch.kanbin.utils.Try;

import java.nio.file.Files;
import java.nio.file.Path;

/**
 * @author Artur Bosch
 */
public class LaneGeneration {

	private static final class Order {

		private static final Order FIRST = new Order(0);
		private static final Order LAST = new Order(-1);
		private static final Order LAST_SPECIAL = new Order(-1);

		private final int index;

		Order(int index) {
			this.index = index;
		}
	}

	private final Project project;
	private final Path projectPath;
	private final DataFormat format;

	public LaneGeneration(Project project, Path projectPath, DataFormat format) {
		this.project = project;
		this.projectPath = projectPath;
		this.format = format;
	}

	public Lane generate(String name, int index) {
		Order order = determineOrder(index);
		Lane lane = Lane.of(name, order == Order.LAST_SPECIAL ? index + 1 : index);
		createLaneFiles(lane);
		updateMovedLanes(order);
		project.addLane(lane);
		return lane;
	}

	public void createLaneFiles(Lane lane) {
		Path lanePath = projectPath.resolve(lane.getName());
		Try.just(() -> Files.createDirectory(lanePath));
		String settingsContent = format.toString(lane);
		Dirs.write(lanePath.resolve(FormatConstants.SETTINGS_FILE), settingsContent);
	}

	private Order determineOrder(int index) {
		int numberOfLanes = project.getLanes().size() - 1;
		Order order;
		if (numberOfLanes < index) {
			order = Order.LAST;
		} else if (index <= 0) {
			order = Order.FIRST;
		} else if (numberOfLanes == project.getLanes().size()) {
			order = Order.LAST_SPECIAL;
		} else {
			order = new Order(index);
		}
		return order;
	}

	private void updateMovedLanes(Order order) {
		int index = order.index;
		if (order != Order.LAST) {
			project.getLanes().stream()
					.filter(current -> current.priority() >= index)
					.forEach(Lane::moveByOne);
		}
	}
}
