package io.gitlab.arturbosch.kanbin.utils;

/**
 * @author Artur Bosch
 */
public class Try<Element> {

	private final Element element;
	private final Exception error;

	private Try(Element element, Exception error) {
		Pre.condition(element == null && error != null || element != null && error == null,
				() -> "A try monad works only if either the element or the object is not null.");
		this.element = element;
		this.error = error;
	}

	public Element get() {
		checkForError();
		return element;
	}

	public <E extends Exception> void thenRun(ConsumerThrowsException<Element, E> consumer) {
		checkForError();
		try {
			consumer.accept(element);
		} catch (Exception e) {
			rethrowError(e);
		}
	}

	public <R, E extends Exception> Try<R> then(FunctionThrowsException<Element, R, E> function) {
		checkForError();
		SupplierThrowsException<R, E> supplier = () -> function.apply(element);
		return Try.to(supplier);
	}

	private void checkForError() {
		if (element == null) {
			Pre.condition(error != null, () -> "Expected error part was unexpectedly null.");
			rethrowError(error);
		}
	}

	private static void rethrowError(Exception error) {
		KanbinError kanbinError = new KanbinError(error);
		kanbinError.setStackTrace(error.getStackTrace());
		throw kanbinError;
	}

	public static <Element, Error extends Exception> Try<Element> to(SupplierThrowsException<Element, Error> todo) {
		try {
			return new Try<>(todo.get(), null);
		} catch (Exception throwable) {
			throw new KanbinError(throwable);
		}
	}

	public static <Element, Error extends Exception> Element just(SupplierThrowsException<Element, Error> todo) {
		try {
			return todo.get();
		} catch (Exception throwable) {
			throw new KanbinError(throwable);
		}
	}

	@FunctionalInterface
	public interface ConsumerThrowsException<T, E extends Exception> {
		void accept(T t) throws E;
	}

	@FunctionalInterface
	public interface SupplierThrowsException<R, E extends Exception> {
		R get() throws E;
	}

	@FunctionalInterface
	public interface FunctionThrowsException<T, R, E extends Exception> {
		R apply(T t) throws E;
	}
}
