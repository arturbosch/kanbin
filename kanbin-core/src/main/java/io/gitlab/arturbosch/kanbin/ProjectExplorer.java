package io.gitlab.arturbosch.kanbin;

import io.gitlab.arturbosch.kanbin.formats.DataFormat;
import io.gitlab.arturbosch.kanbin.models.Lane;
import io.gitlab.arturbosch.kanbin.models.Project;
import io.gitlab.arturbosch.kanbin.utils.Pre;
import io.gitlab.arturbosch.kanbin.utils.Try;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Artur Bosch
 */
public final class ProjectExplorer {

	private final Path basePath;
	private final DataFormat format;

	public ProjectExplorer(Path basePath, DataFormat format) {
		this.basePath = Pre.notNull(basePath);
		this.format = Pre.notNull(format);
	}

	public VirtualProject create(String projectName, Set<String> laneNames) {
		Pre.notEmpty(projectName);
		Pre.condition(laneNames.size() > 1, () -> "Number of lanes must be greater than one.");
		Pre.condition(Files.notExists(basePath.resolve(projectName)),
				() -> "Project with name '" + projectName + "' already exists.");
		HashSet<Lane> lanes = createPrioritizedLanes(laneNames);
		Project project = new Project(projectName, lanes);
		return new VirtualProject(basePath, project, format).generate();
	}

	private HashSet<Lane> createPrioritizedLanes(Set<String> laneNames) {
		int counter = 0;
		HashSet<Lane> lanes = new HashSet<>();
		for (String laneName : laneNames) {
			lanes.add(Lane.of(laneName, counter));
			counter++;
		}
		return lanes;
	}

	public VirtualProject move(VirtualProject project, String newName) {
		Pre.notEmpty(newName);
		Pre.notNull(project);
		Path newPath = basePath.resolve(newName);
		Pre.condition(Files.notExists(newPath),
				() -> String.format("A project with name '%s' already exists.", newName));
		Project projectModel = project.getProject();
		projectModel.updateName(newName);
		Try.to(() -> Files.move(project.getPath(), newPath));
		return new VirtualProject(basePath, projectModel, format);
	}

	public VirtualProject load(String name) {
		Pre.notEmpty(name);
		Path projectPath = basePath.resolve(name);
		Pre.condition(Files.exists(projectPath) && Files.isDirectory(projectPath),
				() -> "Only existing directory can be loaded as a kanbin project.");
		return new VirtualProject(basePath, format.fromFile(projectPath), format);
	}
}
