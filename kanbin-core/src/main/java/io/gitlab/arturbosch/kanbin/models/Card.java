package io.gitlab.arturbosch.kanbin.models;

import io.gitlab.arturbosch.kanbin.utils.Pre;
import io.gitlab.arturbosch.kanbin.utils.Util;

import java.util.UUID;

/**
 * @author Artur Bosch
 */
public class Card {

	private String uuid;
	private String title;
	private String text;
	private String lane;

	@SuppressWarnings("unused")
	Card() {
		// why jackson, why ...
	}

	public Card(String title, String text, String lane) {
		this(UUID.randomUUID().toString(), title, text, lane);
	}

	public Card(String uuid, String title, String text, String lane) {
		Pre.notNull(text, lane, title);
		Pre.notEmpty(uuid);
		this.uuid = uuid;
		this.title = title;
		this.text = text;
		this.lane = lane;
	}

	public void updateLane(Lane newLane) {
		Pre.notNull(newLane);
		this.lane = newLane.getName();
	}

	public void updateCard(String newTitle, String newText, String newLane) {
		Pre.notEmpty(newTitle, newLane);
		Pre.notNull(newText);
		this.title = newTitle;
		this.text = newText;
		this.lane = newLane;
	}

	public String getUuid() {
		return uuid;
	}

	public String getTitle() {
		return title;
	}

	public String getText() {
		return text;
	}

	public String getLane() {
		return lane;
	}

	@Override
	public String toString() {
		return "Card{" +
				"uuid='" + uuid + '\'' +
				", title='" + title + '\'' +
				", text='" + text + '\'' +
				", lane='" + lane + '\'' +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Card card = (Card) o;

		return uuid != null ? uuid.equals(card.uuid) : card.uuid == null;
	}

	@Override
	public int hashCode() {
		return uuid != null ? uuid.hashCode() : 0;
	}

	public Card newCardWithNewContentDefaultToOldIfEmpty(Card newCard) {
		return new Card(
				Util.isEmpty(newCard.title) ? title : newCard.title.trim(),
				Util.isEmpty(newCard.text) ? text : newCard.text.trim(),
				Util.isEmpty(newCard.lane) ? lane : newCard.lane.trim()
		);
	}
}
