package io.gitlab.arturbosch.kanbin.models;

import io.gitlab.arturbosch.kanbin.utils.Pre;
import org.jetbrains.annotations.Nullable;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Artur Bosch
 */
public class Project {

	private String name;
	private Set<Lane> lanes;

	public Project(String name) {
		this(name, new HashSet<>());
	}

	public Project(String name, Set<Lane> lanes) {
		this.name = name;
		this.lanes = lanes;
	}

	public void updateName(String newName) {
		this.name = Pre.notEmpty(newName);
	}

	public void addLane(Lane lane) {
		Pre.notNull(lane);
		Pre.condition(lanes.stream().noneMatch(other -> Objects.equals(other.getName(), lane.getName())),
				() -> String.format("Lane with name '%s' already exists.", lane.getName()));
		lanes.add(lane);
	}

	public void removeLane(Lane toRemove) {
		removeLane(toRemove, null);
	}

	public void removeLane(Lane toRemove, @Nullable Lane toMoveCards) {
		Pre.condition(lanes.contains(toRemove),
				() -> String.format("Lane with name '%s' does not exist in project and can't be removed therefore.",
						toRemove.getName()));
		if (toMoveCards != null) {
			for (Card card : toRemove.getCards()) {
				toMoveCards.addCard(card);
			}
		}
		lanes.remove(toRemove);
	}

	public Optional<Lane> findByName(String laneName) {
		return lanes.stream()
				.filter(lane -> lane.getName().equals(laneName))
				.findFirst();
	}

	public boolean containsByName(String laneName) {
		return lanes.stream().anyMatch(lane -> lane.getName().equals(laneName));
	}

	@Override
	public String toString() {
		return "Project{" +
				"name='" + name + '\'' +
				", lanes=" + lanes +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Project project = (Project) o;

		return name != null ? name.equals(project.name) : project.name == null;
	}

	@Override
	public int hashCode() {
		return name != null ? name.hashCode() : 0;
	}

	public String getName() {
		return name;
	}

	public List<Lane> getLanes() {
		return Collections.unmodifiableList(lanes.stream().sorted(
				Comparator.comparing(lane -> lane.getSettings().getPriority(),
						Integer::compareTo)).collect(Collectors.toList()));
	}
}
