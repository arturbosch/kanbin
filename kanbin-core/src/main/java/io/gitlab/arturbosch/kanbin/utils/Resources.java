package io.gitlab.arturbosch.kanbin.utils;

import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * @author Artur Bosch
 */
public final class Resources {

	private Resources() {
	}

	public static URL get(String name) {
		Pre.notEmpty(name);
		String resource = name.startsWith("/") ? name : "/" + name;
		URL url = Resources.class.getResource(resource);
		return Pre.notNull(url);
	}

	public static Path asPath(String name) {
		try {
			return Paths.get(get(name).toURI());
		} catch (URISyntaxException e) {
			throw new MissingResource(name);
		}
	}

	static class MissingResource extends RuntimeException {
		public MissingResource(String name) {
			super(String.format("No resource with name %s found.", name));
		}
	}
}
