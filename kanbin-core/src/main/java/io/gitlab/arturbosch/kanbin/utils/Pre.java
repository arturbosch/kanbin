package io.gitlab.arturbosch.kanbin.utils;

import java.util.function.Supplier;

/**
 * @author Artur Bosch
 */
public interface Pre {

	static boolean isEmpty(String text) {
		return text == null || text.isEmpty();
	}

	static String notEmpty(String text) {
		if (text == null || text.isEmpty()) {
			throw new IllegalArgumentException("Given value must not be empty.");
		}
		return text;
	}

	static void notEmpty(String text, String... other) {
		notEmpty(text);
		for (String string : other) {
			notEmpty(string);
		}
	}

	static <T> T notNull(T object) {
		if (object == null) {
			throw new IllegalArgumentException("Given object must not be null.");
		}
		return object;
	}

	@SafeVarargs
	static <T> void notNull(T object, T... other) {
		notNull(object);
		for (T obj : other) {
			notNull(obj);
		}
	}

	static void condition(boolean condition, Supplier<String> message) {
		if (!condition) {
			throw new IllegalArgumentException(message.get());
		}
	}

	static void state(boolean condition, Supplier<String> message) {
		if (!condition) {
			throw new IllegalStateException(message.get());
		}
	}
}
