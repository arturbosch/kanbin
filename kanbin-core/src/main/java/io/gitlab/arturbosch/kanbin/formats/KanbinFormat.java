package io.gitlab.arturbosch.kanbin.formats;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import io.gitlab.arturbosch.kanbin.models.Card;
import io.gitlab.arturbosch.kanbin.models.Lane;
import io.gitlab.arturbosch.kanbin.models.LaneSettings;
import io.gitlab.arturbosch.kanbin.models.Project;
import io.gitlab.arturbosch.kanbin.utils.Pre;
import io.gitlab.arturbosch.kanbin.utils.Try;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Artur Bosch
 */
public class KanbinFormat implements DataFormat {

	public static final KanbinFormat INSTANCE = new KanbinFormat();
	private static final ObjectMapper yaml = new ObjectMapper(new YAMLFactory());

	@Override
	public Project fromFile(Path path) {
		Pre.notNull(path);
		Pre.condition(Files.exists(path),
				() -> String.format("Project '%s' does not exist.", path.getFileName().toString()));

		String projectName = path.getFileName().toString();
		Set<Lane> lanes = Try.just(() -> Files.walk(path))
				.filter(dir -> Files.isDirectory(dir) && !dir.equals(path))
				.filter(dir -> !dir.getFileName().toString().startsWith("."))
				.map(this::loadLane)
				.collect(Collectors.toSet());

		return new Project(projectName, lanes);
	}

	private Lane loadLane(Path path) {
		String name = path.getFileName().toString();
		Path settingsFile = path.resolve(FormatConstants.SETTINGS_FILE);
		Pre.state(Files.exists(settingsFile),
				() -> String.format("The '.settings' file is not present for lane '%s'.", name));
		LaneSettings settings = Try.to(() -> Files.newBufferedReader(settingsFile))
				.then(in -> yaml.readValue(in, LaneSettings.class))
				.get();
		Set<Card> cards = Try.just(() -> Files.walk(path))
				.filter(Files::isRegularFile)
				.filter(FormatConstants.NOT_SETTINGS_FILE)
				.map(this::loadCard)
				.collect(Collectors.toSet());
		return new Lane(name, settings, cards);
	}

	private Card loadCard(Path path) {
		return Try.just(() -> yaml.readValue(Files.newBufferedReader(path), Card.class));
	}

	@Override
	public String toString(Card card) {
		Pre.notNull(card);
		return Try.just(() -> yaml.writeValueAsString(card));
	}

	@Override
	public String toString(Lane lane) {
		Pre.notNull(lane);
		return Try.just(() -> yaml.writeValueAsString(lane.getSettings()));
	}

	@Override
	public Card fromString(String card) {
		Pre.notEmpty(card);
		return Try.just(() -> yaml.readValue(card, Card.class));
	}
}
