package io.gitlab.arturbosch.kanbin.utils;

import org.jetbrains.annotations.Nullable;

/**
 * @author Artur Bosch
 */
public final class Util {

	private Util() {
	}

	public static boolean isEmpty(@Nullable String content) {
		return content == null || content.isEmpty();
	}
}
