# kanbin - Kanban Bin - File-Level-Kanban

- Top-Level folders are projects
- Folders are lanes
- Files are cards

### Usage

- `gradle build`
- `cp kanbin-tinbo-plugin/build/libs/kanbin-x.x.x.jar ~/Tinbo/plugins`
- kanbin plugin is loaded on [Tinbo](https://github.com/arturbosch/Tinbo) start
